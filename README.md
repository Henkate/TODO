# nile-dev

## issues

* Pstore doesn't work, see last changes in drivers/power and defconfig (had to do that due to USB-OTG only working after cold boot)
* Incall audio on DSDS (unconfirmed)
* IMS (unconfirmed)

## manifest

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
  <remote fetch="ssh://git@gitlab.com" name="private-gl" />

  <project name="LineageOS/android_hardware_sony_macaddrsetup" path="hardware/sony/macaddrsetup" remote="github" />

  <project name="nile-dev/TODO" path="nile-dev/TODO" remote="private-gl" revision="master" >
    <linkfile src="picks.sh" dest="picks-nile-dev.sh" />
  </project>
  <project name="nile-dev/proprietary_vendor_sony_nile-common" path="vendor/sony/nile-common" remote="private-gl" />
  <project name="nile-dev/proprietary_vendor_sony_discovery" path="vendor/sony/discovery" remote="private-gl" />
  <project name="nile-dev/proprietary_vendor_sony_pioneer" path="vendor/sony/pioneer" remote="private-gl" />
  <project name="nile-dev/android_kernel_sony_sdm660" path="kernel/sony/sdm660" remote="private-gl" />
  <project name="nile-dev/android_device_sony_nile-common" path="device/sony/nile-common" remote="private-gl" />
  <project name="nile-dev/android_device_sony_discovery" path="device/sony/discovery" remote="private-gl" />
  <project name="nile-dev/android_device_sony_pioneer" path="device/sony/pioneer" remote="private-gl" />
</manifest>
```

## building

```
repo sync
./picks-nile-dev.sh
. build/envsetup.sh
brunch {pioneer|discovery}
```